<?php
namespace Billow\Utilities\Traits;

use Billow\Utilities\Models\SocialLink;

trait IsSocial
{
  public function socialLinks()
  {
    return $this->morphMany(SocialLink::class, 'sociable');
  }
}
