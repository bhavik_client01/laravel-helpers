<?php
namespace Billow\Utilities\Traits;

use Billow\Utilities\Models\Address;

trait HasAddress
{
  public function address()
  {
    return $this->morphOne(Address::class, 'addressable');
  }

  public function addresses()
  {
    return $this->morphMany(Address::class, 'addressable');
  }
}
