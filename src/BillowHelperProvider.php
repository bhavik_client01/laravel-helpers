<?php
namespace Billow\Utilities;

use Illuminate\Support\ServiceProvider;

class BillowHelperProvider extends ServiceProvider
{
  use Provider\RegistersMacros,
      Provider\RegistersBindings;

  public function register()
  {
    $this->registerBindings();
  }

  public function boot()
  {
    $this->registerMacros();
  }
}
